/**
 * @file 	format_time.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Return formated string with current (or uptime) time representation
 */

#include "format_time.h"

uint_fast8_t get_string_time(uint_fast8_t format, char *buffer, size_t size)
{
	if(!buffer)
	{
		return 0;
	}
	uint_fast8_t result = 0;
	time_t current_time = get_time_counter(POSIXTIME_T);
	struct tm *stime = localtime(&current_time);
	const char *strftime_fmt = NULL;
	switch(format)
	{
		case DEFAULT_TIME_FORMAT:
			strftime_fmt = "%Y.%m.%d %X";
			break;
		case POSIX_TIME_FORMAT:
			break;
		case LOG_FILE_NAME_TIME_FORMAT_TIME_FORMAT:
			strftime_fmt = "%Y.%m.%d";
			break;
		case MCDI_TIME_FORMAT:
			strftime_fmt = "%H:%M  %m/%d";
			break;
		case SURGARD_TIME_FORMAT:
			strftime_fmt = "%H:%M:%S-%m/%d";
			break;
		case UPTIME_TIME_FORMAT:
			strftime_fmt = "%H:%M";
			break;
		default:
			break;
	}
	if(strftime_fmt)
	{
		if(strftime(buffer, size, strftime_fmt, stime))
		{
			result = 1u;
		}
	}
	else
	{
		if(snprintf(buffer, size - 1u, "%u", current_time) >= 0)
		{
			result = 1u;
		}
	}

	return result;
}

uint_fast8_t get_string_uptime(char *buffer, size_t size)
{
	if(!buffer)
	{
		return 0;
	}
	uint_fast8_t result = 0;
	time_t uptime = get_uptime();
	char temp_buf[128] = {0};
	char current_time[32] = {0};
	if(!get_string_time(UPTIME_TIME_FORMAT, current_time, sizeof current_time))
	{
		return 0;
	}
	if(uptime < 60u)
	{
		if(snprintf(temp_buf, size - 1u, "%s up %u seconds", current_time, uptime) < size)
		{
			strcpy(buffer, temp_buf);
			result = 1;
		}
	}
	else if(uptime < 3600u)
	{
		if(snprintf(temp_buf, size - 1u, "%s up 00:%-.2u", current_time, uptime / 60u) < size)
		{
			strcpy(buffer, temp_buf);
			result = 1;
		}
	}
	else if(uptime < 86400u)
	{
		time_t hours = uptime / 3600u;
		time_t minutes = (uptime - (hours * 3600u)) / 60u;
		if(snprintf(temp_buf, size - 1u, "%s up %-.2u:%-.2u", current_time, hours, minutes) < size)
		{
			strcpy(buffer, temp_buf);
			result = 1;
		}
	}
	else
	{
		time_t days = uptime / 86400u;
		time_t hours = (uptime - (days * 86400u)) / 3600u;
		time_t minutes = (uptime - (hours * 3600u + days * 86400u)) / 60u;
		if(sprintf(temp_buf, "%s up %u days %-.2u:%-.2u", current_time, days, hours, minutes) < size)
		{
			strcpy(buffer, temp_buf);
			result = 1;
		}
	}
	return result;
}
