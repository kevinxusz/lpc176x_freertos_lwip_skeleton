/**
 * @file 	rfc868_time_sync.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Synchronize time with internet usin RFC868 protocol
 */

#ifndef RFC868TIMESYNC_H_
#define RFC868TIMESYNC_H_

#include "AppConfig.h"
#include "time.h"

/**
 * Start synchronization. Not blocking call. It will be set clock whenever asynchronous task is finished
 * @param priority_server - string with IP or domain name of priority seerver. It it is not available then go to through the list on the Internet
 * @param timezone - representation of timezone @see enum TimeZoneNumber in soft_rtc.h
 * @return 1 if sync task has been started, 0 otherwise
 */
uint_fast8_t sync_time_rfc868(const char *priority_server, int timezone);

#endif /* RFC868TIMESYNC_H_ */
