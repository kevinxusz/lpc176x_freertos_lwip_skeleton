/**
 * @file 	iap.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * In-Application Programmin module for LPC176x
 */

#ifndef IAP_H_
#define IAP_H_

#include "stdint.h"

/**
 * Return codes from IAP procedure. See datasheet
 */
enum error_code {
	CMD_SUCCESS,
	INVALID_COMMAND,
	SRC_ADDR_ERROR,
	DST_ADDR_ERROR,
	SRC_ADDR_NOT_MAPPED,
	DST_ADDR_NOT_MAPPED,
	COUNT_ERROR,
	INVALID_SECTOR,
	SECTOR_NOT_BLANK,
	SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION,
	COMPARE_ERROR,
	BUSY,
	PARAM_ERROR,
	ADDR_ERROR,
	ADDR_NOT_MAPPED,
	CMD_LOCKED,
	INVALID_CODE,
	INVALID_BAUD_RATE,
	INVALID_STOP_BIT,
	CODE_READ_PROTECTION_ENABLED
};

/**
 * Sizes for sectors
 */
#define     FLASH_SECTOR_SIZE_0_TO_15    ( 4 * 1024)
#define     FLASH_SECTOR_SIZE_16_TO_29   (32 * 1024)

/**
 * Forward declaration of array with addrresses for all sectors
 * Index in this array = sector's number, value - it's start address
 */
extern const char * const iap_sector_start_adress[];

/**
 * Read device's serial number
 * @param id - pointer to unsigned int[4] where serial havs to be saved
 * @return error code @see enum error_code
 */
uint32_t iap_read_serial(uint32_t *id);

/**
 * Read device's ID
 * @param d - pointer to unsigned int where id has to be saved
 * @return error code @see enum error_code
 */
uint32_t iap_read_id(uint32_t *d);

/**
 * Prepare sector(s) for write operation
 * @param start - start adress to be prepared
 * @param end - last address. Prepare all betwwen them
 * @return error code @see enum error_code
 */
uint32_t iap_write_prepare(uint32_t start, uint32_t end);

/**
 * Write data from RAM to flash
 * @param src - pointer to buffer to write
 * @param dst - address in flash where buffer has to be written
 * @param size - size of buffer to write in bytes
 * @return error code @see enum error_code
 */
uint32_t iap_write(const uint8_t *src, const uint8_t *dst, size_t size);

/**
 * Check if sectro(s) is blank
 * @param start - start adress to be checked
 * @param end - last address. Check all betwwen them
 * @return error code @see enum error_code
 */
uint32_t iap_blank_check(uint32_t start, uint32_t end);
uint32_t iap_erase(uint32_t start, uint32_t end);
uint32_t iap_compare(const uint8_t *src, const uint8_t *dst, size_t size);
void iap_reinvoke_isp(void);

#endif /* IAP_H_ */
