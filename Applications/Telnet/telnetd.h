/**
 * @file 	telnetd.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Telnet-like server
 */

#ifndef TELNETD_H_
#define TELNETD_H_

#include "AppConfig.h"

#define MAX_LOGIN_PASS_LENGTH 32
#define MIN_LOGIN_PASS_LENGTH 4

#define BLOCK_TIME_AFTER_FAILED_LOGIN 30

/**
 * Start the daemon. Create a task and configure FreeRTOS+CLI
 * @param params - pointer to filled stucture struct telnetd_task_params
 * @return 1 if started, 0 otherwise
 */
uint_fast8_t telnetd_start(void);

#endif /* TELNETD_H_ */
