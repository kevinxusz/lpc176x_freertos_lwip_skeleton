/**
 * @file 	AppConfig.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Header with core includes and defines for applications
 */

#ifndef APPCONFIG_H_
#define APPCONFIG_H_

/*
 * Standard includes
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Peripheral library includes
 */
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_nvic.h"
#include "lpc17xx_emac.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_adc.h"
#include "lpc17xx_wdt.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "timers.h"

/* lwIP core includes */
#include "lwip/opt.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

/* Utils includes. */
#include "FreeRTOS_CLI.h"

#include "Clock/soft_rtc.h"
#include "Clock/format_time.h"
#include "Utils/syslog.h"

/*
 * CMSIS include
 */
#if defined (__USE_CMSIS)
	#include "system_LPC17xx.h"
#endif

//#define APPCONFIG_DO_YOU_LIKE_DOGS //<! define to enable watchdog
#ifdef APPCONFIG_DO_YOU_LIKE_DOGS
	#define WDT_RESET_TIMEOUT (2u * 1000u * 1000u) // 2 seconds
#endif
#define UART0_BAUDRATE 57600 //!< default baudtrate for UART0
#define UART3_BAUDRATE 115200 //!< default baudtrate for UART3
#define USB_VCOM_BAUDRATE 115200 //!< default baudtrate for USB virtual serial port
#define LOCAL_TIMEZONE UTC_P_0300 //!< timezone used when you syncronize the time @see enum TimeZoneNumber in soft_rtc.h

/**
 * Thread-safe printf to default UART. See printf_redirect.c
 */
#define TSPRINTF(...) taskENTER_CRITICAL(); \
					printf(__VA_ARGS__); \
					taskEXIT_CRITICAL()

/**
 * Thread-safe printf only in DEBUG configuration
 */
#ifdef DEBUG
	#define DTSPRINTF TSPRINTF
#else
	#define DTSPRINTF(format, args...) ((void)0)
#endif

/**
 * Setup application only when it started from not 0 address (i.e. bootloader is used)
 */
//#define USE_BOOTLOADER
#ifdef USE_BOOTLOADER
	#define APPLICATION_START_ADDRESS 0x8000
	#define REALLOCATE_VECTOR_TABLE (NVIC_SetVTOR(APPLICATION_START_ADDRESS))
#else
	#define REALLOCATE_VECTOR_TABLE ((void)0)
#endif

/**
 * Defined in main.c
 */
extern void soft_reset(void);

#endif /* APPCONFIG_H_ */
